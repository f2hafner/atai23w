/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.persistence.domain;

import com.rcll.domain.Base;
import com.rcll.domain.Cap;
import com.rcll.domain.RingColor;
import com.visualization.domain.VisualizationOrder;
import lombok.*;
import org.robocup_logistics.llsf_msgs.OrderInfoProtos;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductOrder implements VisualizationOrder {

    @Id
    private Long id;

    private OrderInfoProtos.Order.Complexity complexity;

    private long deliveryPeriodBegin;

    private long deliveryPeriodEnd;

    private long deliveryGate;

    @Enumerated(EnumType.STRING)
    private Base baseColor;

    @Enumerated(EnumType.STRING)
    private Cap capColor;

    @Transient
    private List<RingColor> ringColors;

    @Enumerated(EnumType.STRING)
    private RingColor ring1;

    @Enumerated(EnumType.STRING)
    private RingColor ring2;

    @Enumerated(EnumType.STRING)
    private RingColor ring3;

    private long quantityRequested;

    private long quantityDeliveredCyan;

    private long quantityDeliveredMagenta;

    private boolean competitive;

    private boolean delivered;

}
