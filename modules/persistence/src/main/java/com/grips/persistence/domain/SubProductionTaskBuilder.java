package com.grips.persistence.domain;

import com.rcll.domain.MachineSide;

public class SubProductionTaskBuilder {

    private SubProductionTaskBuilder() {}

    public static SubProductionTaskBuilder newBuilder() {
        return new SubProductionTaskBuilder();
    }

    public SubProductionTask build() {
        return new SubProductionTask(
                name,
                machine,
                state,
                type,
                side,
                orderInfoId,
                requiredColor,
                optCode,
                isDemandTask);
    }
    private String name = null;
    private String machine = null;
    private SubProductionTask.TaskState state = null;
    private MachineSide side = null;
    private Long orderInfoId = null;
    private SubProductionTask.TaskType type = null;
    private String requiredColor = null;
    private String optCode = null;
    private boolean isDemandTask = false;

    public SubProductionTaskBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public SubProductionTaskBuilder setMachine(String machine) {
        this.machine = machine;
        return this;
    }

    public SubProductionTaskBuilder setState(SubProductionTask.TaskState state) {
        this.state = state;
        return this;
    }

    public SubProductionTaskBuilder setOrderInfoId(Long orderInfoId) {
        this.orderInfoId = orderInfoId;
        return this;
    }

    public SubProductionTaskBuilder setType(SubProductionTask.TaskType type) {
        this.type = type;
        return this;
    }

    public SubProductionTaskBuilder setSide(MachineSide side) {
        this.side = side;
        return this;
    }

    public SubProductionTaskBuilder setRequiredColor(String requiredColor) {
        this.requiredColor = requiredColor;
        return this;
    }

    public SubProductionTaskBuilder setOptCode(String optCode) {
        this.optCode = optCode;
        return this;
    }

    public SubProductionTaskBuilder setIsDemandTask(boolean isDemandTask) {
        this.isDemandTask = isDemandTask;
        return this;
    }
}
