package com.grips.persistence.domain;

import com.rcll.domain.MachineName;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
public class WorldKnowledge {
    Boolean cs1Buffered;
    Boolean cs2Buffered;
    //todo add RobotPositions!

    Map<Long, MachineName> robotAtMachine;

    public WorldKnowledge() {
        this.cs1Buffered = false;
        this.cs2Buffered = false;
        robotAtMachine = new ConcurrentHashMap<>();
    }

    public void setRobotAtMachine(Long robotId, MachineName machineName) {
        this.robotAtMachine.put(robotId, machineName);
    }
}
