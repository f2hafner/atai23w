package com.visualization.domain;

public interface VisualizationBeaconSignal {
    long getLocalTimestamp();
    String getRobotName();

    //todo change to team name enum!
    String getTeamName();
    String getTask();

    Integer getRobotId();
}
