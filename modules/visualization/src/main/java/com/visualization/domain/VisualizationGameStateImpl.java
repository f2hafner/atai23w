package com.visualization.domain;

import com.rcll.domain.GamePhase;
import com.rcll.domain.TeamColor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VisualizationGameStateImpl implements VisualizationGameState {
    String teamCyan;
    String teamMagenta;
    long pointsCyan;
    long pointsMagenta;
    long gameTimeNanoSeconds;
    com.rcll.domain.GamePhase state;
    GamePhase phase;
    TeamColor gripsColor;
}
