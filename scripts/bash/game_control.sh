echo "Starting game control script..."
phase='INIT'
refbox=$(docker ps | grep refbox)
while [ -z "$refbox" ]
do
    echo "Refbox not up yet, waiting for it to start..."
    refbox=$(docker ps | grep refbox)
done
while [ "$phase" != "PRE_GAME" ]
do
    phase=$(wget -q -O /dev/stdout http://localhost:8090/game/phase)
    echo "=========> Current phase: $phase <==========="
    echo "Waiting for teamserver to be ready!"
    refboxStr=$(docker ps | grep refbox)
    if [ -z "$refboxStr" ]
    then
        echo Refbox is not running anymore, stopping script!
        ./stop.sh
        exit
    fi
    sleep 5;
done
echo "GOT into PRE_GAME, creating game field!"
docker exec refbox /usr/local/bin/rcll-refbox-instruct -s RUNNING
#waiting for gazebo to spawn machines
sleep 5
docker exec refbox /usr/local/bin/rcll-refbox-instruct -p EXPLORATION
sleep 10
lastTime=0
time=1
while [[ "$phase" != "POST_GAME" ]] && [[ "$lastTime" != "$time" ]]
do
    lastTime=$time
    phase=$(wget -q -O /dev/stdout http://localhost:8090/game/phase)
    echo "=========> Current phase: $phase <==========="
    sleep 5;
    time=$(wget -q -O /dev/stdout http://localhost:8090/game/time)
    echo "time:$time, lasttime:$lastTime, phase: $phase"
done
echo "Phase now: $phase, stopping simulation!"
#wait in case any last second order has to be confirmed!
sleep 5
docker exec grips_gazebo bash -c "source /setup_env.sh && rosservice call /gazebo/pause_physics \"{}\""
#docker cp grips_gazebo:/root/.ros/simulation.mp4 simulation.mp4
#docker cp grips_gazebo:/root/.ros/simulation_tmp.mp4 simulation.mp4
sleep 1
./stop.sh
