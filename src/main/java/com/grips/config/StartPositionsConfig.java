package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "startpositions")
@Getter
@Setter
public class StartPositionsConfig {
    private TeamColor cyan;
    private TeamColor magenta;
}