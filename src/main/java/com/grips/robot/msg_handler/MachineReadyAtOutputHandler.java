package com.grips.robot.msg_handler;

import com.rcll.domain.MachineName;
import com.rcll.domain.MachineState;
import com.rcll.refbox.RefboxClient;
import com.robot_communication.services.GripsRobotClient;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.GripsPrepareMachineProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class MachineReadyAtOutputHandler implements Consumer<GripsPrepareMachineProtos.GripsMachineReadyAtOutput>  {
    private final RefboxClient refboxClient;
    private final GripsRobotClient robotClient;


    public MachineReadyAtOutputHandler(RefboxClient refboxClient,
                                    GripsRobotClient robotClient) {
        this.refboxClient = refboxClient;
        this.robotClient = robotClient;
    }

    @Override
    @SneakyThrows
    public void accept(GripsPrepareMachineProtos.GripsMachineReadyAtOutput readyAtOutput) {
        MachineState state = this.refboxClient.getStateForMachine(new MachineName(readyAtOutput.getMachineId()).asMachineEnum())
                .orElseThrow(() -> new RuntimeException("Unkown Machine: " + readyAtOutput.getMachineId()));
        boolean isReadyAtOutput = true;
        boolean failTask = false;//todo think if IDLE is smart!
        if (state.equals(MachineState.IDLE)) {
            Thread.sleep(2000);
            state = this.refboxClient.getStateForMachine(new MachineName(readyAtOutput.getMachineId()).asMachineEnum())
                    .orElseThrow(() -> new RuntimeException("Unkown Machine: " + readyAtOutput.getMachineId()));;
        }
        if (state.equals(MachineState.PROCESSING) || state.equals(MachineState.PREPARED)) {
            isReadyAtOutput = false;
        } else if (state.equals(MachineState.BROKEN)) {
            failTask = true;
            isReadyAtOutput = false;
        } else { //READY_AT_OUTPUT and all other states!
            isReadyAtOutput = true;
        }
        log.info("Machine: " + readyAtOutput.getMachineId() + "failTask: " + failTask + " is readyAtOutput: " + isReadyAtOutput + " state: " + state);
        GripsPrepareMachineProtos.GripsMachineReadyAtOutput msg = GripsPrepareMachineProtos.GripsMachineReadyAtOutput.newBuilder()
                .setRobotId(readyAtOutput.getRobotId())
                .setMachineId(readyAtOutput.getMachineId())
                .setFailTask(failTask)
                .setMachineReadyAtOutput(isReadyAtOutput)
                .build();
        robotClient.sendToRobot(readyAtOutput.getRobotId(), msg);
    }
}
