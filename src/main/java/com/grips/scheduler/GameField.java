package com.grips.scheduler;

import com.grips.config.GameFieldConfig;
import com.rcll.domain.MachineName;
import com.grips.persistence.domain.ExplorationZone;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.rcll.domain.MachinePosition;
import com.rcll.domain.TeamColor;
import com.rcll.domain.ZoneName;
import com.shared.domain.Point2d;
import com.shared.domain.ZoneExplorationState;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@CommonsLog
public class GameField {
    private final GameFieldConfig gameFieldConfig;
    private List<ExplorationZone> _zones = new ArrayList<>();
    private List<ExplorationZone> _waitingPositions = new ArrayList<>();
    private boolean _occupancyInitialized = false;

    private int waitingZoneCounter = 0;

    public GameField(GameFieldConfig gameFieldConfig) {
        List<Integer> startingZones = new ArrayList<>();
        int halfFieldWidth = (gameFieldConfig.getWidth() / 2);
        startingZones.add((halfFieldWidth - 0) * 10 + 1);
        startingZones.add((halfFieldWidth - 1) * 10 + 1);
        startingZones.add((halfFieldWidth - 2) * 10 + 1);


        this.gameFieldConfig = gameFieldConfig;
        List<Integer> insertionZones = new ArrayList();
        Arrays.asList(this.gameFieldConfig.getInsertionZones().split(","))
                .forEach(str -> insertionZones.add(Integer.parseInt(str)));
        for (int width_it = 1; width_it <= (this.gameFieldConfig.getWidth() / 2); ++width_it) {
            for (int height_it = 1; height_it <= this.gameFieldConfig.getHeight(); ++height_it) {
                ExplorationZone currZoneM = new ExplorationZone("M", height_it, width_it,
                        this.gameFieldConfig.getWidth(), this.gameFieldConfig.getHeight(), insertionZones);
                ExplorationZone currZoneC = new ExplorationZone("C", height_it, width_it,
                        this.gameFieldConfig.getWidth(), this.gameFieldConfig.getHeight(), insertionZones);

                currZoneM.setMirroredZone(currZoneC);
                currZoneC.setMirroredZone(currZoneM);

                currZoneM.setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.FREE);
                currZoneC.setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.FREE);

                if (startingZones.stream().filter(i -> i == currZoneM.getZoneNumber()).count() == 0) {
                    // generated zone must qnot be in invalid zones list
                    _zones.add(currZoneM);
                    _zones.add(currZoneC);
                    log.info("generated zone: " + currZoneM.getZoneNumber());
                }
                else {
                    log.info("did not add zone with number " + currZoneM.getZoneNumber());
                }
            }
        }
    }

    private void setBlockedIfExists(long zoneNumber) {
        Optional<ExplorationZone> any = getAllZones().stream().filter(ez -> ez.getZoneNumber() == zoneNumber).findAny();
        if (any.isPresent()) {
            any.get().setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.BLOCKED);
            any.get().getMirroredZone().setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.BLOCKED);
        }
    }

    public void updateHalfFieldOccupancy(List<MachineInfoRefBox> mInfos, String teamPrefix) {
        if (_occupancyInitialized || mInfos == null) {
            return;
        }
        for (MachineInfoRefBox machine : mInfos) {
            ExplorationZone z = getZoneByName(machine.getZone());
            z.setMachine(machine.getName());
            if (z.getZoneOccupiedState() == ExplorationZone.ZoneOccupiedState.MACHINE) {
                continue;
            }
            log.debug("Updating field: " + machine + " in " + z.getZoneName());
            z.setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.MACHINE);
            if (machine.getZone().startsWith(teamPrefix)) {
                // machine is in our halffield
                long zoneNumber = z.getZoneNumber();
                if (machine.getRotation() == 0 || machine.getRotation() == 180) {
                    long leftZoneNumber = zoneNumber - 10;
                    long rightZoneNumber = zoneNumber + 10;

                    if (leftZoneNumber > 0) {
                        setBlockedIfExists(leftZoneNumber);
                    }
                    if (rightZoneNumber < ((gameFieldConfig.getWidth()/2)+1)*10) {
                        setBlockedIfExists(rightZoneNumber);
                    }
                } else if (machine.getRotation() == 90 || machine.getRotation() == 270) {
                    long upperZoneNumber = zoneNumber + 1;
                    long lowerZoneNumber = zoneNumber - 1;

                    if (upperZoneNumber%10 <= gameFieldConfig.getHeight()) {
                        setBlockedIfExists(upperZoneNumber);
                    }
                    if (lowerZoneNumber%10 > 0) {
                        setBlockedIfExists(lowerZoneNumber);
                    }
                } else if (machine.getRotation() == 45 || machine.getRotation() == 225 || machine.getRotation() == 135 || machine.getRotation() == 315) {
                    // these zones are allways blocked
                    long upperZoneNumber = zoneNumber+1;
                    long lowerZoneNumber = zoneNumber-1;
                    long leftZoneNumber = zoneNumber-10;
                    long rightZoneNumber = zoneNumber+10;

                    if (leftZoneNumber > 0) {
                        setBlockedIfExists(leftZoneNumber);
                    }
                    if (rightZoneNumber < ((gameFieldConfig.getWidth()/2)+1)*10) {
                        setBlockedIfExists(rightZoneNumber);
                    }
                    if (upperZoneNumber%10 <= gameFieldConfig.getHeight()) {
                        setBlockedIfExists(upperZoneNumber);
                    }
                    if (lowerZoneNumber%10 > 0) {
                        setBlockedIfExists(lowerZoneNumber);
                    }

                    if (machine.getRotation() == 135 || machine.getRotation() == 315) {
                        long upperrightZone = zoneNumber+11;
                        long lowerleftZone =  zoneNumber-11;

                        if (upperrightZone%10 <= gameFieldConfig.getHeight() && upperrightZone < ((gameFieldConfig.getWidth()/2)+1)*10) {
                            setBlockedIfExists(upperrightZone);
                        }
                        if (lowerleftZone%10 > 0 && lowerleftZone > 0) {
                            setBlockedIfExists(lowerleftZone);
                        }
                    } else if (machine.getRotation() == 45 || machine.getRotation() == 225) {
                        long upperleftZone = zoneNumber-10+1;
                        long lowerrightZone =  zoneNumber+10-1;

                        if (upperleftZone%10 <= gameFieldConfig.getHeight() && upperleftZone > 0) {
                            setBlockedIfExists(upperleftZone);
                        }
                        if (lowerrightZone%10 > 0 && lowerrightZone < ((gameFieldConfig.getWidth()/2)+1)*10) {
                            setBlockedIfExists(lowerrightZone);
                        }
                    }
                }
            }
        }

        //generateWaitingPositions(teamPrefix);
        //calculateWaitingZones();
        _occupancyInitialized = true;
    }

    private void generateWaitingPositions(String teamPrefix) {
        for (int x = 1; x <= (gameFieldConfig.getWidth()/2); ++x) {
            for (int y = 1; y <= (gameFieldConfig.getHeight()); ++y) {
                String zoneName = teamPrefix + "_Z" + (x * 10 + y);
                ExplorationZone z = getZoneByName(zoneName);
                if (z != null && z.getZoneOccupiedState() == ExplorationZone.ZoneOccupiedState.FREE) {
                    // zone is free, check if surrounding can be used for path

                    List<ExplorationZone> surroundingZones = new ArrayList<>();

                    String zName;
                    ExplorationZone z1 = null;
                    ExplorationZone z2 = null;
                    ExplorationZone z3 = null;
                    if (x > 1) {
                        // we are mirrored, so "x=0" has the same state as x
                        zName = teamPrefix + "_Z" + ((x - 1) * 10 + (y - 1));
                        z1 = getZoneByName(zName);
                        if (z1 != null) {
                            surroundingZones.add(z1);
                        }
                        zName = teamPrefix + "_Z" + ((x - 1) * 10 + (y));
                        z2 = getZoneByName(zName);
                        if (z2 != null) {
                            surroundingZones.add(z2);
                        }
                        zName = teamPrefix + "_Z" + ((x - 1) * 10 + (y + 1));
                        z3 = getZoneByName(zName);
                        if (z3 != null) {
                            surroundingZones.add(z3);
                        }
                    }

                    zName = teamPrefix + "_Z" + ((x) * 10 + (y-1));
                    ExplorationZone z4 = getZoneByName(zName);
                    if (z4 != null) {
                        surroundingZones.add(z4);
                    }
                    zName = teamPrefix + "_Z" + ((x) * 10 + (y+1));
                    ExplorationZone z5 = getZoneByName(zName);
                    if (z5 != null) {
                        surroundingZones.add(z5);
                    }

                    zName = teamPrefix + "_Z" + ((x+1) * 10 + (y-1));
                    ExplorationZone z6 = getZoneByName(zName);
                    if (z6 != null) {
                        surroundingZones.add(z6);
                    }
                    zName = teamPrefix + "_Z" + ((x+1) * 10 + (y));
                    ExplorationZone z7 = getZoneByName(zName);
                    if (z7 != null) {
                        surroundingZones.add(z7);
                    }
                    zName = teamPrefix + "_Z" + ((x+1) * 10 + (y+1));
                    ExplorationZone z8 = getZoneByName(zName);
                    if (z8 != null) {
                        surroundingZones.add(z8);
                    }

                    if (surroundingZones.stream().allMatch(n -> n.getZoneOccupiedState() == ExplorationZone.ZoneOccupiedState.FREE || n.getZoneOccupiedState() == ExplorationZone.ZoneOccupiedState.BLOCKED)) {
                        // all surroundings are free, use the zone for waiting
                        _waitingPositions.add(z);
                    }

                    if (surroundingZones.size() > 5 && surroundingZones.stream().filter(n -> n.getZoneOccupiedState() == ExplorationZone.ZoneOccupiedState.MACHINE).count() == 1) {
                        // only one machine in our surroundings (if we are not at a wall), we use this for waiting
                        _waitingPositions.add(z);
                    }
                }
            }
        }

        if (_waitingPositions.size() < 3) {
            List<ExplorationZone> mirroredWaiting = new ArrayList<>();
            for (ExplorationZone e : _waitingPositions) {
                mirroredWaiting.add(e.getMirroredZone());
            }
            _waitingPositions.addAll(mirroredWaiting);
        }

        log.info("----------Waiting positions--------");
        for (ExplorationZone z : _waitingPositions) {
            log.info(z.getZoneName());
        }

        Collections.shuffle(_waitingPositions);
    }

    public String getWaitingZone(int robotId) {
        int idx = waitingZoneCounter % this._waitingPositions.size();
        waitingZoneCounter++;
        return _waitingPositions.get(idx).getZoneName() + "_waiting";
    }

    public List<ExplorationZone> getAllZones() {
        return _zones;
    }

    public ExplorationZone getZoneByName(String zoneName) {
        Optional<ExplorationZone> zone = _zones.stream()
                .filter(z -> z.getZoneName().equalsIgnoreCase(zoneName))
                .findAny();
        if (zone.isPresent()) {
            return zone.get();
        }
        log.error("Cannot find zone: " + zoneName);
        return null;
    }

    public static int rotateMachineIfNecessary(int xPos, int yPos, MachineName name, int rotation) {
        boolean nextToWall = false;
        if (yPos == 1 || yPos == 8 || xPos == 7 || (yPos == 2 && xPos == 6)) {
            nextToWall = true;
        }

        rotation = (180 - rotation);
        rotation = (rotation % 360 + 360) % 360;
        if (nextToWall &&  (name.isCapStation()|| name.isRingStation())) {
            rotation = (rotation + 180) % 360;
        }

        return rotation;
    }

    public void markExploredByPosition(Point2d pose) {
        this._zones.stream()
                .filter(zone -> zone.getZoneCenter().distance(pose) < 0.25) //todo play around with this value!
                .filter(zone -> zone.getExplorationState() != ZoneExplorationState.EXPLORED)
                .forEach(zone -> {
                    log.info("Marking zone " + zone.getZoneName() + " as EXPLORED");
                    zone.setExplorationState(ZoneExplorationState.EXPLORED);
                });
    }

    private List<ExplorationZone> getNeigboursForZone(ExplorationZone zone) {
        List<ExplorationZone> neigbours = new ArrayList<>();
        if (zone.getHeightIDX() > 1) {
            this._zones.stream()
                    .filter(x -> x.getZonePrefix().equals(zone.getZonePrefix()))
                    .filter(x -> x.getHeightIDX() == zone.getHeightIDX() - 1 && x.getWidthIDX() == zone.getWidthIDX())
                    .forEach(neigbours::add);
        }
        if (zone.getHeightIDX() < this.gameFieldConfig.getHeight()) {
            this._zones.stream()
                    .filter(x -> x.getZonePrefix().equals(zone.getZonePrefix()))
                    .filter(x -> x.getHeightIDX() == zone.getHeightIDX() + 1 && x.getWidthIDX() == zone.getWidthIDX())
                    .forEach(neigbours::add);
        }
        if (zone.getWidthIDX() == 1) {
            this._zones.stream()
                    .filter(x -> !x.getZonePrefix().equals(zone.getZonePrefix()))
                    .filter(x -> x.getWidthIDX() == zone.getWidthIDX() && x.getHeightIDX() == zone.getHeightIDX())
                    .forEach(neigbours::add);
        } else {
            this._zones.stream()
                    .filter(x -> x.getZonePrefix().equals(zone.getZonePrefix()))
                    .filter(x -> x.getWidthIDX() == zone.getWidthIDX() - 1 && x.getHeightIDX() == zone.getHeightIDX())
                    .forEach(neigbours::add);
        }
        if (zone.getWidthIDX() < this.gameFieldConfig.getWidth() / 2) {
            this._zones.stream()
                    .filter(x -> x.getZonePrefix().equals(zone.getZonePrefix()))
                    .filter(x -> x.getWidthIDX() == zone.getWidthIDX() + 1 && x.getHeightIDX() == zone.getHeightIDX())
                    .forEach(neigbours::add);
        }
        return neigbours;
    }

    public void calculateWaitingZones(TeamColor teamPrefix) {
        log.info("Calculating new waiting zones");
        String colorPrefix = "C";
        switch (teamPrefix){
            case CYAN:
                colorPrefix = "C";
                break;
            case MAGENTA:
                colorPrefix = "M";
                break;
        }
        String color = colorPrefix;
        this._waitingPositions.clear();
        this._zones.stream()
                .filter(x -> x.getZonePrefix().equals(color))
                .filter(x -> !x.getZoneOccupiedState().equals(ExplorationZone.ZoneOccupiedState.MACHINE))
                .forEach(zone -> {
            if (getNeigboursForZone(zone).stream()
                    .noneMatch(x -> x.getZoneOccupiedState().equals(ExplorationZone.ZoneOccupiedState.MACHINE))) {
                this._waitingPositions.add(zone);
            }
        });
        log.info("Found new waiting zones");
        this._waitingPositions.forEach(x -> log.info("WaitingZone: " + x.getZoneName()));
    }

    public void updateMachineZones(Map<MachineName, MachinePosition> machineZones) {
        List<ZoneName> zones = machineZones.values().stream().map(MachinePosition::getZone).collect(Collectors.toList());
        this._zones.forEach(x -> {
            if (zones.contains(new ZoneName(x.getZoneName()))) {
                x.setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.MACHINE);
                x.getMirroredZone().setZoneOccupiedState(ExplorationZone.ZoneOccupiedState.MACHINE);
            }
        });
    }
}
