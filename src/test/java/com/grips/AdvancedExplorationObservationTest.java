/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class AdvancedExplorationObservationTest {
/*
    private static final long ROBOT_ID = 1;
    private static final long ZONE_NUMBER = 3;
    private static final double SUM_CONF = 1.0;
    private static final String MACHINE_NAME = "C-CS1";
    private static final long OBS_TIME = 1000;
    private AdvancedExplorationObservation observation;

    @BeforeEach
    void setUp() {
        observation = new AdvancedExplorationObservation(ROBOT_ID, "Z" + ZONE_NUMBER, SUM_CONF, MACHINE_NAME, OBS_TIME);
    }

    @Test
    void getMachineName() {
        assertEquals(MACHINE_NAME, observation.getMachineName());
    }

    @Test
    void setMachineName() {
        final String machineNameNew = "M-DS";
        observation.setMachineName(machineNameNew);
        assertEquals(machineNameNew, observation.getMachineName());
    }

    @Test
    void getObservationTime() {
        assertEquals(OBS_TIME, observation.getObservationTime());
    }

    @Test
    void setObservationTime() {
        final long obsTimeNew = 123456;
        observation.setObservationTime(obsTimeNew);
        assertEquals(obsTimeNew, observation.getObservationTime());
    }

    @Test
    void getZoneNumber() {
        assertEquals(ZONE_NUMBER, observation.getZoneNumber());
    }

    @Test
    void setZoneNumber() {
        final long zoneNumberNew = 23;
        observation.setZoneNumber(zoneNumberNew);
        assertEquals(zoneNumberNew, observation.getZoneNumber());
    }


    @Test
    void getRobotId() {
        assertEquals(ROBOT_ID, observation.getRobotId());
    }

    @Test
    void setRobotId() {
        final long robotIdNew = 3;
        observation.setRobotId(robotIdNew);
        assertEquals(robotIdNew, observation.getRobotId());
    }

    @Test
    void getZone() {
        assertEquals("Z" + ZONE_NUMBER, observation.getZone());
    }

    @Test
    void setZone() {
        final String zoneNew = "Z20";
        observation.setZone(zoneNew);
        assertEquals(zoneNew, observation.getZone());
    }

    @Test
    void getSumConf() {
        assertEquals(SUM_CONF, observation.getSumConf());
    }

    @Test
    void setSumConf() {
        final double sumConfNew = 0.5;
        observation.setSumConf(sumConfNew);
        assertEquals(sumConfNew, observation.getSumConf());
    }

    @Test
    void testDefaultConstructor() {
        AdvancedExplorationObservation testObs = new AdvancedExplorationObservation();
        assertNotEquals(null, testObs);
    }
*/
}