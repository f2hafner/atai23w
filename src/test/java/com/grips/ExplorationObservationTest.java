/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips;

import com.grips.persistence.domain.ExplorationObservation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ExplorationObservationTest {

    private static final double SUM_CONF = 1.0;
    private static final String ZONE = "Z2";
    private static final long ROBOT_ID = 2;
    private ExplorationObservation observation;

    @BeforeEach
    void setUp() {
        observation = new ExplorationObservation();
        observation.setConfidence(SUM_CONF);
        //observation.setZone(ZONE);
        observation.setRobotId(ROBOT_ID);
    }

    @Test
    void getRobotId() {
        assertEquals(ROBOT_ID, observation.getRobotId());
    }

    @Test
    void setRobotId() {
        long newRobotId = 3;
        observation.setRobotId(newRobotId);
        assertEquals(newRobotId, observation.getRobotId());
    }

    @Test
    @Disabled
    void getZone() {
        assertEquals(ZONE, observation.getZone());
    }

    @Test
    @Disabled
    void setZone() {
        String newZoneName = "Z23";
        //observation.setZone(newZoneName); todo fix
        assertEquals(newZoneName, observation.getZone());
    }

    @Test
    void getSumConf() {
        assertEquals(SUM_CONF, observation.getConfidence());
    }

    @Test
    void setSumConf() {
        double newSumConf = 0.6;
        observation.setConfidence(newSumConf);
        assertEquals(newSumConf, observation.getConfidence());
    }

}