import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


export interface IOrder {
  id: number
  requested: number
  delivered: number
  deliveryPeriodBegin: number
  deliveryPeriodEnd: number
  deliveryGate: number
  cap: string
  ring1?: Ring
  ring2?: Ring
  ring3?: Ring
  base: string
  competitive: boolean
  complexity: string
}

export interface Ring {
  machine: string
  color: string
  rawMaterial: number
}

export interface IVisualization{
  observations: any;
  robot1: any;
  robot2: any;
  robot3: any;
  plan: string;
  timeFromPlan: number;
  timeStamp: number;
  productOrders: IOrder[];
}

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  constructor(private httpClieht: HttpClient) { }

  public fetchData(): Observable<IVisualization> {
    return this.httpClieht.get<IVisualization>("http://localhost:8080/visualization");
  }
}
